## Prerequies

Have an ansible ready directory, with a working inventory.

Like the one generate by [Air Init](https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/init)

Here is an example of what it should look like:

```
inventory
├── hosts
└── host_vars
    ├── your.first.hostname
    │   ├── 001_sudo_vault.yml
    │   ├── 001_vault.yml
    │   ├── 042_host.yml
    │   ├── 042_sudo.yml
    │   ├── 042_user.yml
    │   ├── 999_pass.yml
    │   └── 999_user.yml
    ├── your.second.hostname
        ├── 001_sudo_vault.yml
        ├── 001_vault.yml
        ├── 042_host.yml
        ├── 042_sudo.yml
        ├── 042_user.yml
        ├── 999_pass.yml
        └── 999_user.yml

.ssh/
├── config
├── id_ed25519_ansible
├── id_ed25519_ansible.pub
└── known_hosts
```

All host_vars are vars witch configure ssh and sudo parameter for ansible.

```
ansible_host
ansible_ssh_user
ansible_become
ansible_become_password
...

```




## Copy Init Inventory File
```
cp -r ../Init/.ssh ../Init/.vault_password ../Init/inventory .
```
## Remove Useless File
```
find inventory/ -name 001_vault.yml -exec rm {} \; # as the only vault we use now is for sudo
find inventory/ -name 042_vault.yml -exec rm {} \; # as we will only the user set in 999_user.yml
```
## Clean Ssh Config
```
rm .ssh/known_hosts
rm .ssh/config
chmod 700 .ssh
chmod 600 .ssh/*
```

## Test If Ansible is working
```
ansible all -i inventory -m setup -a 'gather_subset=!all'
```
## Install Base System
```
./install.sh
```

#### Check Locale
```
ansible all -i inventory -m raw -a "source /etc/profile.d/locale.sh && locale"
```

#### Check Log File
```
ansible all -i inventory -m raw -a "ls /var/log"
ansible all -i inventory -m raw -a "tail /var/log/everything.log"
```

#### Check Iptables
```
ansible all -i inventory -m raw -a "iptables -nvL"
ansible all -i inventory -m raw -a "ip6tables -nvL"
```

#### Check Fail2ban
```
ansible all -i inventory -m raw -a "fail2ban-client status sshd"
```


#### Check Dns and Network
```
ansible all -i inventory -m raw -a "ping -c 3 wikipedia.fr"
ansible all -i inventory -m raw -a "wget wikipedia.fr -O /dev/null"
```


## Install Databases
You need to add some host in the inventory group:
- redis
- mariadb
- postgresql
```
ansible-playbook -i inventory ./playbooks/004_install_databases.yml
```
#### Check Database
```
ansible redis -i inventory -m raw -a "redis-cli --version"
ansible mariadb -i inventory -m raw -a "mysql --version"
ansible postgresql -i inventory -m raw -a "psql --version"
```
