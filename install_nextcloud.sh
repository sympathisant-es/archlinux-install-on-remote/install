#!/usr/bin/env bash

set -e

ansible-playbook -v -i inventory ./playbooks/013_install_nextcloud.yml
