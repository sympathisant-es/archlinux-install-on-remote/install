#!/usr/bin/env bash

set -e

ansible-playbook -v -i inventory ./playbooks/000_*.yml
ansible-playbook -v -i inventory ./playbooks/001_*.yml
ansible-playbook -v -i inventory ./playbooks/002_*.yml
ansible-playbook -v -i inventory ./playbooks/003_*.yml
ansible-playbook -v -i inventory ./playbooks/004_*.yml
